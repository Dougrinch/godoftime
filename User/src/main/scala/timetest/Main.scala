package timetest

import java.text.{SimpleDateFormat, DateFormat}
import java.util.{Date, TimeZone}
import org.dou.got.TimeSetter._

object Main extends App {


    System.out.println(new Date())
    setTime(0)
    System.out.println(new Date())
    revertTime()
    System.out.println(new Date())


    //absolute_time_order

    printDate() // 0
    setTime(0) // 1
    printDate() // 2

    thread("good") {
        printDate() // 3*
        Thread.sleep(900)
        printDate() // 13
    }

    thread("_bad") {
        printDate() // 3*
        setTime(5000) // 4
        Thread.sleep(250)
        printDate() // 5
        revertTime() // 6
        printDate() // 7
        setTime(5000) // 8
        printDate() // 9
        Thread.sleep(750)
        printDate() // 14
        revertTime() // 15
        printDate() // 16
    }

    Thread.sleep(750)
    printDate() // 10

    revertTime() // 11

    printDate() // 12

    def printDate() {
        val format: DateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss.SSS")
        format.setTimeZone(TimeZone.getTimeZone("UTC"))
        System.out.println(Thread.currentThread.getName + ": " + format.format(new Date))
    }

    def thread(name: String)(body: => Unit) {
        new Thread(new Runnable {
            override def run(): Unit = body
        }, name).start()
    }
}
