package org.dou.got;

import java.io.IOException;
import java.io.InputStream;
import java.lang.instrument.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class PreMain {

    private static final String TRANSFORMER_CLASS = "org.dou.got.TimeTransformer";

    private static final List<String> CLASSPATH_JARS = Arrays.asList(
            "got-api-1.0.jar",
            "got-transformer-1.0.jar",
            "asm-5.0.jar"
    );

    @SuppressWarnings("unchecked")
    public static void premain(String agentArgs, Instrumentation inst)
            throws UnmodifiableClassException, IOException, ClassNotFoundException, IllegalAccessException, InstantiationException, IllegalClassFormatException {

        List<URL> urls = new ArrayList<URL>();

        for (String jar : CLASSPATH_JARS) {
            URL url = Thread.currentThread().getContextClassLoader().getResource(jar);
            if (url != null)
                urls.add(url);
        }

        InnerJarClassLoader innerJarClassLoader = new InnerJarClassLoader(urls);
        Class<ClassFileTransformer> transformerClass = (Class<ClassFileTransformer>) innerJarClassLoader.findClass(TRANSFORMER_CLASS);
        ClassFileTransformer transformer = transformerClass.newInstance();

        inst.addTransformer(transformer);

        redefine(inst, transformer);
    }

    private static void redefine(Instrumentation inst, ClassFileTransformer transformer) throws IllegalClassFormatException, ClassNotFoundException, UnmodifiableClassException {
        ArrayList<Class> classes = new ArrayList<Class>();
        HashSet<Class> done = new HashSet<Class>();
        FastByteBuffer buf = new FastByteBuffer();
        while (true) {
            classes.addAll(Arrays.asList(inst.getAllLoadedClasses()));
            List<ClassDefinition> cdl = new ArrayList<ClassDefinition>(classes.size());
            for (Class clazz : classes) {
                if (clazz.isArray())
                    continue;
                if (!done.add(clazz))
                    continue;
                String name = clazz.getName().replace('.', '/');
                InputStream is = clazz.getResourceAsStream("/" + name + ".class");
                buf.clear();
                if (is != null)
                    try {
                        try {
                            buf.readFrom(is);
                        } finally {
                            is.close();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                if (buf.isEmpty()) {
                    continue;
                }
                byte[] result = transformer.transform(
                        clazz.getClassLoader(), name, clazz, clazz.getProtectionDomain(), buf.getBytes());
                if (result != null)
                    cdl.add(new ClassDefinition(clazz, result));
            }
            classes.clear();
            if (cdl.isEmpty())
                break;

            inst.redefineClasses(cdl.toArray(new ClassDefinition[cdl.size()]));
        }
    }
}
