package org.dou.got;

public class TimeManager {

    private static final ThreadLocal<TimeManager> time = new InheritableThreadLocal<TimeManager>() {

        @Override
        protected TimeManager initialValue() {
            return new TimeManager(null);
        }

        @Override
        protected TimeManager childValue(TimeManager parentValue) {
            return new TimeManager(parentValue);
        }
    };

    public static void setTime(long millis) {
        time.get().setMyTime(millis);
    }

    public static long getTime() {
        return time.get().getMyTime();
    }

    public static void revertTime() {
        time.get().revertMyTime();
    }

    private final TimeManager parent;
    private long delta;
    private boolean useParentTime;

    public TimeManager(TimeManager parent) {
        this.parent = parent;
        this.delta = 0;
        this.useParentTime = true;
    }

    public void setMyTime(long millis) {
        if (millis >= 0) {
            useParentTime = false;
            delta = millis - System.currentTimeMillis();
        } else if (millis == -1)
            revertMyTime();
        else
            throw new IllegalArgumentException("millis must be positive or -1 for revertTime");
    }

    public long getMyTime() {
        if (parent != null && useParentTime)
            return parent.getMyTime();

        return System.currentTimeMillis() + delta;
    }

    public void revertMyTime() {
        useParentTime = true;
        delta = 0;
    }
}
