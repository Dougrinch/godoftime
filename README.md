# GodOfTime

## WTF
    System.out.println(new Date())
    setTime(0)
    System.out.println(new Date())
    revertTime()
    System.out.println(new Date())
will print

    Sat Jun 21 03:58:18 MSK 2014
    Thu Jan 01 03:00:00 MSK 1970
    Sat Jun 21 03:58:18 MSK 2014
    
This tool NOT modified system time.


## Use
1. run app with "-javaagent:got-agent-1.0.jar"
2. add "got-api-1.0.jar" to classpath
3. use methods 
   * godoftime.TimeSetter.setTime(long)
   * godoftime.TimeSetter.revertTime()


## Example
[Main.scala](https://github.com/Dougrinch/godoftime/blob/master/User/src/main/scala/timetest/Main.scala)
