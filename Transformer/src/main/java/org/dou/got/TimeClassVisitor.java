package org.dou.got;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.Opcodes.ASM5;

public class TimeClassVisitor extends ClassVisitor {

    public static final String INTERNAL_TIME_MANAGER_NAME = "org/dou/got/TimeManager";
    public static final String INTERNAL_TIME_SETTER_NAME = "org/dou/got/TimeSetter";

    private String className;

    public TimeClassVisitor(ClassVisitor cv) {
        super(ASM5, cv);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(version, access, name, signature, superName, interfaces);
        className = name;
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        MethodVisitor mv = super.visitMethod(access, name, desc, signature, exceptions);

        if (mv == null || className.equals(INTERNAL_TIME_MANAGER_NAME))
            return mv;

        return new TimeMethodVisitor(mv);
    }
}
