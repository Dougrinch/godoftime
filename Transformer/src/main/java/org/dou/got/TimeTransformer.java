package org.dou.got;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import static org.objectweb.asm.ClassWriter.COMPUTE_MAXS;

public class TimeTransformer implements ClassFileTransformer {

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined, ProtectionDomain protectionDomain, byte[] classfileBuffer) throws IllegalClassFormatException {
        return transform(new ClassReader(classfileBuffer));
    }

    private static byte[] transform(ClassReader reader) {
        ClassWriter writer = new ClassWriter(reader, COMPUTE_MAXS);
        TimeClassVisitor visitor = new TimeClassVisitor(writer);
        reader.accept(visitor, 0);
        return writer.toByteArray();
    }
}
