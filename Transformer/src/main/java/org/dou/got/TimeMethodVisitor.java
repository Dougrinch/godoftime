package org.dou.got;

import org.objectweb.asm.MethodVisitor;

import static org.dou.got.TimeClassVisitor.INTERNAL_TIME_MANAGER_NAME;
import static org.dou.got.TimeClassVisitor.INTERNAL_TIME_SETTER_NAME;
import static org.objectweb.asm.Opcodes.ASM5;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;
import static org.objectweb.asm.Type.*;

public class TimeMethodVisitor extends MethodVisitor {

    public TimeMethodVisitor(MethodVisitor mv) {
        super(ASM5, mv);
    }

    @Override
    public void visitMethodInsn(int opcode, String owner, String name, String desc, boolean itf) {
        if (isSetTimeMethod(opcode, owner, name, desc)) {
            replaceSetTimeMethod();
        } else if (isRevertTimeMethod(opcode, owner, name, desc)) {
            replaceRevertTimeMethod();
        } else if (isCurrentTimeMillisMethod(opcode, owner, name, desc)) {
            replaceCurrentTimeMillisMethod();
        } else
            super.visitMethodInsn(opcode, owner, name, desc, itf);
    }

    private boolean isSetTimeMethod(int opcode, String owner, String name, String desc) {
        if (opcode != INVOKESTATIC)
            return false;

        if (!owner.equals(INTERNAL_TIME_SETTER_NAME))
            return false;

        if (!name.equals("setTime") || !desc.equals(getMethodDescriptor(VOID_TYPE, LONG_TYPE)))
            return false;

        return true;
    }

    private void replaceSetTimeMethod() {
        visitMethodInsn(INVOKESTATIC, INTERNAL_TIME_MANAGER_NAME, "setTime", getMethodDescriptor(VOID_TYPE, LONG_TYPE), false);
    }

    private boolean isRevertTimeMethod(int opcode, String owner, String name, String desc) {
        if (opcode != INVOKESTATIC)
            return false;

        if (!owner.equals(INTERNAL_TIME_SETTER_NAME))
            return false;

        if (!name.equals("revertTime") || !desc.equals(getMethodDescriptor(VOID_TYPE)))
            return false;

        return true;
    }

    private void replaceRevertTimeMethod() {
        visitMethodInsn(INVOKESTATIC, INTERNAL_TIME_MANAGER_NAME, "revertTime", getMethodDescriptor(VOID_TYPE), false);
    }

    private boolean isCurrentTimeMillisMethod(int opcode, String owner, String name, String desc) {
        if (opcode != INVOKESTATIC)
            return false;

        if (!owner.equals(getInternalName(System.class)))
            return false;

        if (!name.equals("currentTimeMillis") || !desc.equals(getMethodDescriptor(LONG_TYPE)))
            return false;

        return true;
    }

    private void replaceCurrentTimeMillisMethod() {
        visitMethodInsn(INVOKESTATIC, INTERNAL_TIME_MANAGER_NAME, "getTime", getMethodDescriptor(LONG_TYPE), false);
    }
}
